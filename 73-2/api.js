const express = require('express');

const api = express();
const port = 9000;

const Vigenere = require('caesar-salad').Vigenere;

api.get('/encode/:code',(req,res) =>{
    let encode = Vigenere.Cipher('password').crypt(req.params.code);
    res.send(encode);
})
api.get('/decode/:decode',(req,res) =>{
    let decode = Vigenere.Decipher('password').crypt(req.params.decode);
    res.send(decode);
})

api.listen(port,()=>{
    console.log('API run')
})